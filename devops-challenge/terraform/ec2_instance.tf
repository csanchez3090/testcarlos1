resource "aws_instance" "server1"{
    ami = "${var.ami_id}"
    instance_type = "t3a.nano"
    count = 1
    associate_public_ip_address = true
    subnet_id = "${aws_subnet.subnet1.id}"    
    vpc_security_groups_id = ["${aws_security_groups.sg-1.id}","${aws_security_groups.sg-2.id}"]
    private_ip = "11.22.33.44"
    key_name = "class_key1"
    user_data = "${file("userdata.sh")}"

    Tags = {
        Names = "server1"
        Owner = "terraform"
        Env = "dev"
    }    
}

/* resource "aws_instance" "server2"{
    ami = "ami-0fc61db8544a617ed"
    instance_type = "t2.micro"
    count = 1
    associate_public_ip_address = true
    subnet_id = "${aws_subnet.subnet1.id}"    
    vpc_security_groups_id = ["${aws_security_groups.sg-1.id}","${aws_security_groups.sg-2.id}"]
    private_ip = "10.0.10.11"
    key_name = "${aws_key_pair.key-class2.id}"
    user_data = <<EOF
    #!/bin/bash
export PATH=$PATH: /urs/local/bin
sudo -i
exect > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
yum install -y httpd
echo "<html><h1>Hola, Exelente ejercicio.......<p></p><p>Continuamos!!!</h2></html>" > /var/www/html/index.html
systemctl start httpd
systemctl enable httpd
echo "Hola Mundo" > hola.txt
EOF
    Tags = {
        Names = "server2"
        Owner = "terraform"
        Env = "dev"
    }    
} */