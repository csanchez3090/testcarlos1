provider "aws " {
  region = "${var.region}"
  access_key="---------" #segun orga
  secret_key="---------" #segun orga
}
resource "aws_vpc" "main1" {
  cidr_block = "${var.vpc_cidr}"
  tags = {
    Name = "Main1"
  }
}
resource "aws_subnet" "subnet1" {
  vpc_id = "${aws_vpc.main1.id}"
  cidr_block = "${var.subnet1_cidr}"
  map_public_ip_on_launch = true
  availability_zone = "${var.subnet1_zone_1a}"
  tags {
    name = "subnet-pub-a"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id = "${aws_vpc.main1.id}"
  cidr_block = "${var.subnet2_cidr}"
  map_public_ip_on_launch = true
  availability_zone = "${var.subnet2_zone_1b}"
  tags {
    name = "subnet-pub-b"
  }
}

resource "aws_subnet" "subnet3" {
  vpc_id = "${aws_vpc.main1.id}"
  cidr_block = "${var.subnet3_cidr}"
  map_public_ip_on_launch = true
  availability_zone = "${var.subnet3_zone_1c}"
  tags {
    name = "subnet-pub-c"
  }
}
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main1.id}"
  tags {
    name = "main"
  }
}
resource "aws_route_table" "r" {
  vpc_id = "${aws_vpc.main1.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  tags{
    name = "rt-main"
  }
  }
}
resource "aws_route_table_association" "table_subnet1" {
  subnet_id = "${aws_subnet.subnet1.id}"
  route_table_id = "${aws_route_table.r.id}"
}

resource "aws_route_table_association" "table_subnet2" {
  subnet_id = "${aws_subnet.subnet2.id}"
  route_table_id = "${aws_route_table.r.id}"
}

resource "aws_route_table_association" "table_subnet3" {
  subnet_id = "${aws_subnet.subnet3.id}"
  route_table_id = "${aws_route_table.r.id}"
}