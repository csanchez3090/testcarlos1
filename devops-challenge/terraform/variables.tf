variable "region" {
  description = "Region a utilizar en AWS"
  default = "us-east-1"
}

variable "vpc_cidr" {
  description = "VPC cidr"
  default = "10.0.0.0/16"
}

variable "subnet1_cidr" {
  description = "Subnet 1 cidr"
  default = "10.0.10.0/24"
}
variable "subnet2_cidr" {
  description = "Subnet 2 cidr"
  default = "10.0.20.0/24"
}
variable "subnet3_cidr" {
  description = "Subnet 3 cidr"
  default = "10.0.30.0/24"
}

variable "subnet1_zone_1a" {
  description = "Zona 1a - Subnet 1"
  default = "us-east-1a"
}

variable "subnet2_zone_1b" {
  description = "Zona 1b - Subnet 2"
  default = "us-east-1b"
}

variable "subnet3_zone_1c" {
  description = "Zona 1c - Subnet 3"
  default = "us-east-1c"
}


variable "ami_id" {
  description = "AMI id para RHEL8"
  #type = string
  default = "ami-0fc61db8544a617ed"
  
}
